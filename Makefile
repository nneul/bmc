all:
	mkdir -p obj
	./compile

tidy:
	for f in src/*; do (cd $$f && ../../jsontidy); done